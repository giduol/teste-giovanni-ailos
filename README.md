**Desafio API Ailos**

Testes automatizados dos serviços disponibilizados pela url: https://serverest.dev/.

**Objetivo:**

• Validar usuário cadastrado com sucesso;<br>
• Validar verificações realizadas no cadastro de usuário;<br>
• Validar Edição de um usuário;<br>
• Validar listagem de usuários;<br>
• Validar Exclusão de um usuário.<br>

**Recursos utilizados:**

• Java 11; <br>
• Rest Assured; <br>
• Maven; <br>
• Testng. <br>

**Como configurar o ambiente:**

• Faça clone do projeto: https://gitlab.com/giduol/teste-giovanni-ailos.git; <br>
• Importe o projeto para sua IDE de preferência;<br>
 • Necessário ter instalado o JDK11. <br>

**Como executar a aplicação:**

• src/test/java/testcases: para rodar os testes de cadastro, consultas, edição e exclusão de usuários;<br>
• src/test/java/healthcheck: para rodar a validação de healthcheck;<br>
• src/test/java/testeContrato: para rodar a validação de contrato.<br>

**Relatórios:**

A aplicação gera os relatórios automaticamente e pode ser visualizado no arquivo html: relatorio\execucao.html

**Pipeline:**

Os testes estão rodando na pipeline do Gitlab CI/CD conforme configurado no gitlab-ci.yml.

package testcases;

import DTO.Usuario;
import helper.BaseTest;;
import io.restassured.path.json.JsonPath;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ConsultarUsuario extends BaseTest {

    @Test
    public void realizarConsulta() {
        Usuario usuario = new Usuario();

        String token = steps.gerarToken();
        JsonPath insercao = steps.inserirCadastro(token, usuario);

        JsonPath consulta = steps.buscarCadastro(insercao.getString("_id"));
        Assert.assertEquals(insercao.getString("_id"), consulta.getString("_id"));
        Assert.assertEquals(consulta.getString("nome"), usuario.getNOME());
        Assert.assertEquals(consulta.getString("email"), usuario.getEMAIL());
        Assert.assertEquals(consulta.getString("password"), usuario.getPASSWORD());
        Assert.assertEquals(consulta.getString("administrador"), usuario.getADMINISTRADOR());
        steps.excluirCadastro(insercao.getString("_id"));
    }
}








package testcases;

import helper.BaseTest;
import helper.Steps;
import io.restassured.path.json.JsonPath;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ConsultarListaUsuario extends BaseTest {

    @Test
    public void listarUsuarios(){
        JsonPath listaUsuarios = steps.buscarListaUsuarios();
        Assert.assertTrue(listaUsuarios.getInt("quantidade") > 0);
    }
}








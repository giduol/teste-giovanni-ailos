package testcases;

import DTO.Usuario;
import helper.BaseTest;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ExcluirUsuario extends BaseTest {

    @Test
    public void realizarExclusao() {
        String token = steps.gerarToken();

        Usuario usuario = new Usuario();

        String mensagemExclusao = steps.excluirCadastro(steps.inserirCadastro(token, usuario).getString("_id"));
        Assert.assertEquals(mensagemExclusao, "Registro excluído com sucesso");
    }
}








package testcases;

import DTO.Usuario;
import helper.BaseTest;
import io.restassured.path.json.JsonPath;
import org.testng.Assert;
import org.testng.annotations.Test;

public class InserirUsuario extends BaseTest {

    @Test()
    public void realizarCadastro() {
        String token = steps.gerarToken();
        Usuario usuario = new Usuario();
        JsonPath insercao = steps.inserirCadastro(token, usuario);
        Assert.assertEquals(insercao.getString("message"), "Cadastro realizado com sucesso");
        steps.excluirCadastro(insercao.getString("_id"));
    }
}







